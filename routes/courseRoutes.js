const express = require("express");
const router = express.Router();
const auth = require("../auth")

const courseControllers =  require("../controllers/courseControllers.js");

// Adding course
router.post("/",courseControllers.addCourse);

module.exports=router;