const Course = require("../models/courses");
const Users = require("../models/user");
const auth=require("../auth");

/*
	Steps:
	1. Create a new Course object using mongoose model and information from the request of the user
	2. Save the new course to the database
*/

module.exports.addCourse = (request,response) => {
	
	const isAdmin = auth.decode(request.headers.authorization).isAdmin; 
		
	if(isAdmin){
		let newCourse = new Course({
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			slots:request.body.slots
		})

		newCourse.save().then((result,isAdmin) => {
			console.log(result);
			response.send(true);
		}).catch(error =>{
			console.log(error);
			response.send(false);
		})
	}else{
		return response.send(`User must be ADMIN to access this.`);
	}
}


